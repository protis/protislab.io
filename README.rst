
.. image:: https://img.shields.io/badge/website-protis.gitlab.io-8c4ab0.svg?labelColor=dedede&style=for-the-badge&logo=data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDBtbSIgaGVpZ2h0PSI0MG1tIiB2aWV3Qm94PSIwIDAgNDAgNDAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTIwLjE5MS40NjEgOC42OTggMTEuOTU0djE2LjE3NWwxMS40OTMgMTEuNDkzIDMuMjkzLTMuMjkyVjIyLjcyNmgxMy42MDNsMi42ODQtMi42ODRMMjAuMTkxLjQ2MnptLS4zOCA3Ljk0IDYuNSA2LjUtNi41IDYuNS02LjUtNi41IDYuNS02LjV6TTUuNzQ3IDE0LjkwNS42MSAyMC4wNDFsNS4xMzcgNS4xMzhWMTQuOTA0eiIgZmlsbD0iIzhjNGFiMCIvPjwvc3ZnPg==
 :alt: protis
 :target: https://protis.gitlab.io

.. image:: https://img.shields.io/gitlab/pipeline/protis/protis.gitlab.io/main?logo=gitlab&labelColor=dedede&style=for-the-badge
  :target: https://gitlab.com/protis/protis.gitlab.io/commits/main
  :alt: pipeline status

.. image:: https://img.shields.io/website-up-down-green-red/https/protis.gitlab.io/?logo=firefox&logoColor=white&style=for-the-badge
 :target: https://protis.gitlab.io
 :alt: site up down

.. .. image:: https://img.shields.io/badge/download-PDF-blue?color=c8615e&logo=read-the-docs&logoColor=white&style=for-the-badge
..   :target: https://protis.gitlab.io/_downloads/protis.pdf
..   :alt: license


.. image:: https://img.shields.io/badge/license-GPLv3-blue?color=5faa9c&logo=open-access&logoColor=white&style=for-the-badge
 :target: https://gitlab.com/protis/protis.gitlab.io/-/blob/main/LICENCE.txt
 :alt: license



Website for protis
==================


This repository contains scripts for building the documentation
website `https://protis.gitlab.io <https://protis.gitlab.io>`_.
